#include "usart.h"

#ifndef __MICROLIB
#define __FILE_INCOMPLETE 1
#endif
#include <stdio.h>

#ifndef __MICROLIB
struct __FILE
{
  int handle;
  /* Whatever you require here. If the only file you are using is */
  /* standard output using printf() for debugging, no file handling */
  /* is required. */
}__stdout;
#endif

#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)

PUTCHAR_PROTOTYPE
{
 /* e.g. write a character to the LPUART1 and Loop until the end of transmission */
 HAL_UART_Transmit(&hlpuart1, (uint8_t *)&ch, 1, 0xFFFF);
 return ch;
}
